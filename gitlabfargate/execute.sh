#!/bin/bash
filename='script_order.txt'
scriptdir="scripts"

#download and unzip
cd $scriptdir
aws2 s3 cp "s3://fargate-1/$CI_FILE" .
unzip -o $CI_FILE

#given the right permission
echo "Creating and setting the permission for the projecgt dir"
sudo mkdir -m 777 -p $CI_PROJECT_DIR
sudo chmod -R 777 "$CI_PROJECT_DIR/.."

#checkinf all files
echo "Using the Project Directory: $( pwd )"
ls -ltr
echo "Using the unix user: $( whoami )"

#executing scripts
while read -r line || [[ $line ]]; do
    echo "Executing $line:"
    bash "./$scriptdir/$line"
done < "$scriptdir/$filename"