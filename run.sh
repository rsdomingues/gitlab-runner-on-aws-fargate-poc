#!/bin/bash
#specific configurations for your AWS Account.
AWS_ACCOUNT_ID="YOUR_AWS_ACCOUNT_ID"
FARGATE_IMAGE="$AWS_ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/gitlabfargate:rc1"
FARGATE_EXECUTION_ROLE="arn:aws:iam::$AWS_ACCOUNT_ID:role/ecsTaskExecutionRole"
FARGATE_SUBNET="subnet-3e73b430"
FARGATE_SECURITY_GROUP="sg-0bce12250c7d3dba7"

#register all scripts names
filename='script_order.txt'
scriptdir="scripts"
CI_FILE="$CUSTOM_ENV_CI_PROJECT_NAME-$CUSTOM_ENV_CI_BUILD_ID.zip"
mkdir -p $scriptdir

#register execution order
echo "${2}.sh" >> $filename
cp -v "${1}" "$scriptdir/${2}.sh"
echo "${2}"

if [ ${2} == "archive_cache" ] 
then
    #copy the order file and zip
    cp $filename $scriptdir
    zip -r $CI_FILE $scriptdir
    
    #upload files to amazon S3 bucket
    aws2 s3 cp $CI_FILE s3://fargate-1

    #configure fargate task
    echo "{\"awsvpcConfiguration\":{\"subnets\":[\"$FARGATE_SUBNET\"],\"securityGroups\":[\"$FARGATE_SECURITY_GROUP\"],\"assignPublicIp\":\"DISABLED\"}}" > network.json
    echo "{\"family\":\"fargateExecutor\",\"networkMode\":\"awsvpc\",\"executionRoleArn\":\"$FARGATE_EXECUTION_ROLE\",\"containerDefinitions\":[{\"name\":\"gitlab-runner\",\"image\":\"$FARGATE_IMAGE\",\"portMappings\":[],\"environment\":[{\"name\":\"CI_FILE\",\"value\":\"$CI_FILE\"},{\"name\":\"CI_PROJECT_DIR\",\"value\":\"$CUSTOM_ENV_CI_PROJECT_DIR\"}],\"essential\":true}],\"requiresCompatibilities\":[\"FARGATE\"],\"cpu\":\"256\",\"memory\":\"512\"}" > task.json
    
    echo "Creating new task definition"
    echo "Task Definition: $(cat task.json)"  
    aws2 ecs register-task-definition --cli-input-json file://task.json > taskDefinition.json
    TASK_DEFINITION_ARN=$( node -pe 'JSON.parse(process.argv[1]).taskDefinition.taskDefinitionArn' "$(cat taskDefinition.json)")
    echo "Task definition created: $TASK_DEFINITION_ARN"

    #execute fargate task
    echo "Starting task on fargate"
    echo "Network Definition: $(cat network.json)"  
    aws2 ecs run-task \
        --cluster GitlabExecutor \
        --network-configuration file://network.json \
        --task-definition $TASK_DEFINITION_ARN > runningTask.json

    TASK_ARN=$( node -pe 'JSON.parse(process.argv[1]).tasks[0].taskArn' "$(cat runningTask.json)")
    
    echo "Task running: $TASK_ARN"

    aws2 ecs wait tasks-stopped --cluster GitlabExecutor --tasks $TASK_ARN

    echo "Task Finished: $TASK_ARN"
    echo "Thanks for using Gitlab AWS Fargate :)"
fi
