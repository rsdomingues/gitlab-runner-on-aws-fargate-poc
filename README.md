# GitLab Runner on AWS Fargate POC

This is a solution for the GitLab Issue [#2972](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972). To implement this solution I've used the GitLab Custom Executor to accumulate the build scripts and send it to AWS Cloud to an S3 Bucket and then start an AWS Fargate task to run the scripts.

This is a PoC to evaluate the feasibility of the solutions there are still elements missing for a real case production use. But it is possible to run a GitLab Build in the AWS Fargate without the need to extend the current Executors.

For simplicity, this PoC uses only shell scripts. And it was created to build a simple [java+maven application](https://gitlab.com/rsdomingues/gitlab_blueprint).


## The solution
![Executor Architecture](/img/arch.png "Executor Architecture")


##### The solution has some basic components:
 - **GitLab Runner (as is)**: running on an EC2 instance (or any machine).
 - **AWS S3 Bucket**: This will be used as a connection to the AWS Fargate container running the build. So every build script and any other files that are required.
 - **AWS ECR**: Used to host the Fargate Gitlab task executor image.
 - **AWS Fargate Cluster**: Used to run the task executor container.
 - **AWS Cloud Watch**: Used to capture the task executor logs to be traced back to the GitLab environment.

##### How the solution it works?

[Install and register](https://docs.gitlab.com/runner/install/) the GitLab Runner on any ec2 instance (or any other machine), since the runner itself will not run any build, you can use something like a T3.micro just to have the connection with the GitLab environment (Cloud or not). This runner should be configured to use the custom executor see the [config.toml](/config.toml)

The custom runner will execute the [run.sh](run.sh) script that is going to accumulate the build steps into several scripts. Once it has accumulated the scripts it will zip and upload them to the AWS S3 Bucket.

After that it will create an AWS Task Definition, using a provided Docker Image that has every necessary tool to execute the GitLab CI/CD Task. This image must be provided and configured on the [run.sh](run.sh) script. 

For this example the image that we are providing has Java and Maven installed, you can check the [Dockerfile](gitlabfargate/Dockerfile) to see. But it is simply using a base image with a lot of techs installed, it is installing the AWS CLI to connect with the AWS S3 and configuring the credentials. There is no need to configure the credentials if you inform an execution role that has access to ECR and S3.

It is necessary to create the Task definition (and updated) at every build because it has the environment variables that we will need for each build. So the script creates and updates it at every execution.

After the task definition is updated then, it runs the task as a stand-alone task (not as a service) on the Aws Fargate. It will wait for the task to end.

##### What do I need to do to run it myself:

 - Created an AWS account.
 - Create an S3-bucket.
 - Create an AWS ECS Cluster Network Only Powered by Fargate.
 - Create an EC2 Instance a T3.Micro is more than enough. You can also use any other machine, but the machine will need AWS CLI 2 installed and access to the AWS API.
 - Install and register the GitLab Runner, use the [config.toml](config.toml) as reference.
 - Configure the [run.sh](run.sh) with the Environment Variables:
    - AWS_ACCOUNT_ID
    - FARGATE_IMAGE
    - FARGATE_EXECUTION_ROLE
    - FARGATE_SUBNET
    - FARGATE_SECURITY_GROUP
 - Configure your GitLab repo to use the runner and be happy :)

![GitLab Job Execution Log](/img/execution_log.png "GitLab Job Execution Log")


##### Next steps

As I mentioned before there are still some elements missing on this POC:
 - Connect to the AWS Cloud Watch Logs to see the execution details
 - Execute the Success or Fail build scripts as needed
 - Handle exceptions on the task execution as a failed build.
